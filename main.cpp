#include<iostream>
#include<stdio.h>
#include<windows.h>
#include<stdlib.h>

#define MAX_N 30

using namespace std;

int a[MAX_N][MAX_N];
int b[MAX_N][MAX_N];
const int n=MAX_N;


void init_input();
void displayHelp();
void init_setup();
void simulate();
void next_gen();
void output();
void pause();
int one_alive();
int sum(int j,int i);

int main(){
    SetConsoleDisplayMode(GetStdHandle(STD_OUTPUT_HANDLE),CONSOLE_FULLSCREEN_MODE,0);
    system("mode con COLS=700");
    init_setup();
    init_input();

    simulate();
}

void simulate(){

    while(one_alive()){

        output();

        next_gen();

        if(GetAsyncKeyState(int('Q')) == -32767)
            exit(0);
        if(GetAsyncKeyState(int('P')) == -32767)
            pause();

        Sleep(100);

        system("cls");
    }

    cout<<"All cells are dead!"<<endl<<endl<<"Press any key to continue...";
    return;
}
void pause(){
    int ind=0;
    while(ind==0){
        if(GetAsyncKeyState(int('R'))==-32767)
            ind=1;
    }
}
void displayHelp(){
    cout<<"Conway's Game of Life v1.2"<<endl<<"-by Kiclu"<<endl<<endl;
    cout<<"Rules: "<<endl;
    cout<<"  1: Simulation area is defined as 30x30 squares, or cells"<<endl;
    cout<<"  2: Cells are defined as either alive or dead, white ones are alive, black ones are dead"<<endl;
    cout<<"  3: A generation is the current state of the simulation"<<endl;
    cout<<"  4: Cells can either die or reproduce"<<endl;
    cout<<"  5: Cells can die of isolation or overpopulation"<<endl;
    cout<<"  6: Cells die of isolation if there is fewer than 2 neighbouring cells that are alive"<<endl;
    cout<<"  7: Cells die of overpopulation if there are more than 4 neighbouring cells that are alive"<<endl;
    cout<<"  8: If a cell is dead, and there are exactly 3 neighbouring cells, cell will come alive in the next generation"<<endl;
    cout<<endl<<endl<<endl;
    cout<<"Controls: "<<endl;
    cout<<"  'P' - pause"<<endl;
    cout<<"  'R' - resume"<<endl;
    cout<<"  'Q' - quit"<<endl;
    cout<<endl<<endl<<endl;
    cout<<"Press any key to continue..."<<endl;
    cin.get();
    cin.get();
    system("cls");
    init_input();
}
void next_gen(){
    int nb=0;
    int k;
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            //nb=0;
            //nb+=a[i-1][j-1]+a[i-1][j]+a[i-1][j+1]+a[i][j-1]+a[i][j+1]+a[i+1][j-1]+a[i+1][j]+a[i+1][j+1];
            nb=sum(j,i);
            if( nb<2 || nb>=4)
                k=0;
            if(nb==3)
                k=1;
            if(nb==2)
                k=a[i][j];
            b[i][j]=k;
        }
    }

    for(int i=0;i<n;i++)
        for(int j=0;j<n;j++)
            a[i][j]=b[i][j];
}
void output(){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            if(a[i][j]==1)
                printf("%c%c",char(219),char(219));
            else
                printf("%c%c",' ',' ');
        }
        printf("\n");
    }
}
void init_setup(){
    for(int i=0;i<n;i++)
        for(int j=0;j<n;j++)
            a[i][j]=b[i][j]=0;
}
int one_alive(){
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++)
            if(a[i][j]==1)
                return 1;
    }
    return 0;
}
void init_input(){
    int t,tx,ty;
    cout<<"Input number of initial cells"<<endl<<"Input 0 for help"<<endl;
    cin>>t;
    system("cls");
    if(t<=0)
        displayHelp();
    else{
        cout<<"Input "<< t << " pair(s) of coordinates."<<endl;
        for(int i=0;i<t;i++){
            scanf("%d%d",&tx,&ty);
            a[ty][tx]=1;
        }
        system("cls");
    }
}
int sum(int j,int i){
    int s=0;
    int x=j;
    int y=i;
    if(x>0)
        s+=a[i][j-1];
    if(x<n-1)
        s+=a[i][j+1];
    if(y>0)
        s+=a[i-1][j];
    if(y<n-1)
        s+=a[i+1][j];
    if(x>0 && y>0)
        s+=a[i-1][j-1];
    if(x<n-1 && y>0)
        s+=a[i-1][j+1];
    if(x>0 && y<n-1)
        s+=a[i+1][j-1];
    if(x<n-1 && y<n-1)
        s+=a[i+1][j+1];

    return s;

}

